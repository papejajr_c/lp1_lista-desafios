//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(int argc, char const *argv[])
{
	char palavra[200];
	char caracter;
	int contadorP = 0;

	char letras[223] = {0};
	int contadorC = 0;
	
	printf("Digite uma frase\n");
	while(caracter != '\n') {
		caracter = getchar();
		
		if(caracter == '\n') { 
			break;
		}
	
		palavra[contadorP] = caracter;
		contadorP++;
	}

	palavra[contadorP] = '\0';

	//contar os caracteres
	while(palavra[contadorC] != '\0')  {
		if(palavra[contadorC] >= ' ' && palavra[contadorC] <= 223) 
			letras[palavra[contadorC] - ' ']++;
		contadorC++;
	}

	//Exibindo
	for (contadorC = 0; contadorC < 223; contadorC++)
	{
		if(letras[contadorC] != 0) {
			if(contadorC + ' ' == ' ')
				printf("espaço=%d\n", letras[contadorC]);
			else
				printf("%c=%d\n", contadorC + ' ', letras[contadorC]);
		}
	}
	return 0;
}
