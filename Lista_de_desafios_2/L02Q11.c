//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	int tamanhoLinha, tamanhoColuna, i, j;

	printf("Digite o tamanho da linha:\n");
	scanf("%d", &tamanhoLinha);

	printf("Digite o tamanho da coluna:\n");
	scanf("%d", &tamanhoColuna);

	int vetor[tamanhoLinha][tamanhoColuna];
	
	for( i = 0; i < tamanhoLinha; i++) {
		for (j = 0; j < tamanhoColuna; j++) {
			scanf("%d", &vetor[i][j]);
		}
	}

	for( i = 0; i < tamanhoLinha; i++) {
		for (j = 0; j < tamanhoColuna; j++) {
			printf("%d, ", vetor[i][j]);
		}
		printf("\n");
	}

	printf("\n");
 	return 0;
}
