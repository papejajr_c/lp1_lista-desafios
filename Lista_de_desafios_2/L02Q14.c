//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	int tamanho, contador1 = 0, contador2 = 0;

	printf("Digite o tamanho do vetor:\n");
	scanf("%d", &tamanho);
	
	int vetor1[tamanho];
	int vetor2[tamanho];

	while(contador1 < tamanho) {
		scanf("%d", &vetor1[contador1]);
		contador1++;
	}
	
	contador1 = 0;
	contador2 = tamanho;

	while(contador1 < tamanho) {
		contador2--;
		vetor2[contador1] = vetor1[contador2];
//		printf("%d:%d \t%d - %d\n", contador1, contador2, vetor1[contador2], vetor2[contador1]);
		contador1++;
	}

	while(contador2 < tamanho) {
		printf("%d, ", vetor2[contador2]);
		contador2++;
	}
	printf("\n");
 	return 0;
}
