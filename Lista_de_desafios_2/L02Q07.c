//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	int numero, divisor = 2;

	printf("Digite o numero:\n");
	scanf("%d", &numero);
	
	if(numero <= 1) {
		printf("Numero não é primo\n");
		return 0;
	} else {
		while(divisor <= numero / 2) {
			if (numero % divisor == 0) {
				printf("Numero não é primo\n");
				return 0;
			}
			divisor = divisor + 1;
		}
	}
	printf("Numero é primo\n");
 	return 0;
}
