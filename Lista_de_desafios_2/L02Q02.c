//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	int numero, contador = 1;
	printf("Digite o primeiro número\n");
	scanf("%d", &numero);
 	
	while(contador <= 10){
		printf("%d * %d = %d\n", numero, contador, numero * contador);
		contador+=1;
	}
 	return 0;
}
