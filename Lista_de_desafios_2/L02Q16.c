//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(int argc, char const *argv[])
{
	char nome[200];
	char caracter;
	int contador = 0;

	printf("Digite uma frase\n");
	while(caracter != '\n') {
		caracter = getchar();
		
		if(caracter == '\n') { 
			break;
		}
		
		nome[contador] = caracter;
		contador++;
	} 
	contador = (unsigned) strlen(nome) ;
	while(contador >= 0) {
		printf("%c", nome[contador--]);
	}
	printf("\n");
	return 0;
}
