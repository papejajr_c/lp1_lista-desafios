//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	int tamanhoLinha0, tamanhoColuna0, tamanhoLinha1, tamanhoColuna1, i, j, maiorLinha, maiorColuna;

	printf("Digite o tamanho da linha:\n");
	scanf("%d", &tamanhoLinha0);

	printf("Digite o tamanho da coluna:\n");
	scanf("%d", &tamanhoColuna0);

	int vetor0[tamanhoLinha0][tamanhoColuna0];
	
	for( i = 0; i < tamanhoLinha0; i++) {
		for (j = 0; j < tamanhoColuna0; j++) {
			scanf("%d", &vetor0[i][j]);
		}
	}

	printf("Digite o tamanho da linha:\n");
	scanf("%d", &tamanhoLinha1);

	printf("Digite o tamanho da coluna:\n");
	scanf("%d", &tamanhoColuna1);

	int vetor1[tamanhoLinha1][tamanhoColuna1];

	for( i = 0; i < tamanhoLinha1; i++) {
		for (j = 0; j < tamanhoColuna1; j++) {
			scanf("%d", &vetor1[i][j]);
		}
	}

	//Saber a maior coluna
	if (tamanhoColuna0 > tamanhoColuna1)
		maiorColuna = tamanhoColuna0;
	else
		maiorColuna = tamanhoColuna1;

	//Saber a maior linha
	if (tamanhoLinha0 > tamanhoLinha1)
		maiorLinha = tamanhoLinha0;
	else
		maiorLinha = tamanhoLinha1;

	int vetor[maiorLinha][maiorColuna];

	//soma as matrizes
	for( i = 0; i < maiorLinha; i++) {
		for (j = 0; j < maiorColuna; j++) {

			if(i < tamanhoLinha0 && j < tamanhoColuna0) {
				if(i < tamanhoLinha1 && j < tamanhoColuna1) {
					vetor[i][j] = vetor0[i][j] + vetor1[i][j];
				} else {
					vetor[i][j] = vetor0[i][j];
				}
			} else {
				vetor[i][j] = vetor1[i][j];
			}

			printf("%d, ", vetor[i][j]);
		}
		printf("\n");
	}

	printf("\n");
 	return 0;
}
