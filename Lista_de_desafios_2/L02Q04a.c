//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	const int senha = 1234;
	int digitado = 0;

	while(1){
		printf("Advinhe a senha:\n");
		scanf("%d", &digitado);
		if(digitado == senha){
			printf("Senha correta\n");
			return 0;
		}
		else{
			printf("Senha incorreta\n");
			continue;
		}
	}
 	return 0;
}
