//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void menu();
void potenciacao();
void raizQuadrada();
void fatorial();

int main(int argc, char const *argv[])
{
	menu();
 	return 0;
}

void menu(){
	int opcao;
	do {
		printf("Menu de Opções\n");
		printf("1 - potenciação\n");
		printf("2 - raiz quadrada\n");
		printf("3 - fatorial\n");
		printf("0 - sair\n");
		printf("Digite a opcao:\n");
		scanf("%d", &opcao);

		if(opcao >= 0 && opcao <=3) {
			switch(opcao) {
				case 1:
					potenciacao();
					printf("-------------------------------------------------\n");
					continue;
				case 2:
					raizQuadrada();
					printf("-------------------------------------------------\n");
					continue;
				case 3:
					fatorial();
					printf("-------------------------------------------------\n");
					continue;
				case 0:
					return;
			}
		} else {
			printf("Numero inválido\n");
			continue;
		}

	} while(1);
}

void raizQuadrada() {
	int numero;
	printf("Digite o numero:\n");
	scanf("%d", &numero);
	printf("Raiz quadrada = %f\n", sqrt(numero));
}

void potenciacao() {
	int numeroBase, numeroExpoente;
	printf("Digite a base:\n");
	scanf("%d", &numeroBase);
	printf("Digite o expoente:\n");
	scanf("%d", &numeroExpoente);
	printf("Potenciação = %f\n", pow(numeroBase, numeroExpoente));
}

void fatorial(){
	int numero, cont, resultado;

	printf("Digite o numero:\n");
	scanf("%d", &numero);
	
	cont = numero-1;

	while(cont >= 1){
		resultado = (numero * cont);
		numero = resultado;
		cont--;
	}
	printf("O fatorial é = %d\n", numero);
}