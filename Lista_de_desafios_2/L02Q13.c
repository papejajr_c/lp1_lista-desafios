//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	int tamanho, contador = 0;

	printf("Digite o tamanho do vetor:\n");
	scanf("%d", &tamanho);
	
	int vetor[tamanho];
		
	while(contador < tamanho) {
		scanf("%d", &vetor[contador]);
		contador++;
	}
	
	while(contador > 0) {
		contador--;
		printf("%d, ", vetor[contador]);
	}
	printf("\n");
 	return 0;
}
