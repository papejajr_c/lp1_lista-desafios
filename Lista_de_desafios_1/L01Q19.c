//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	int numerador, denominador;

 	printf("%s\n", "Digite o numerador");
	scanf("%d", &numerador);

	printf("%s\n", "Digite o denominador");
	scanf("%d", &denominador);
	if(denominador == 0) {
		printf("denominador não pode ser zero\n");
		return 0;
	}
	printf("quociente = %d\n", numerador / denominador);
	printf("resto = %d\n", numerador % denominador);
	
 	return 0;
}
