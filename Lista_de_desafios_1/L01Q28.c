//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char const *argv[])
{	
	float a, b, c, delta, raiz1, raiz2, sqrtdelta;
	
	printf("Digite o valor de a\n");
	scanf("%f", &a);

	printf("Digite o valor de b\n");
	scanf("%f", &b);

	printf("Digite o valor de c\n");
	scanf("%f", &c);

	if( a==0 ) return 0;

	delta = exp(b) - 4 * a * c;
	sqrtdelta = sqrt(delta);

	if(delta >= 0) {
		raiz1 = (-b + sqrtdelta) / (2 * a);
		raiz2 = (-b - sqrtdelta) / (2 * a);
	} else {
		 delta = -delta;
		 raiz1 = (-b)/(2*a), (sqrtdelta)/(2*a);
		 raiz2 = (-b)/(2*a), (sqrtdelta)/(2*a);

	}

	printf("Delta = %.2f\n", delta);
	printf("Raiz de delta = %.2f\n", sqrtdelta);
	printf("Raiz1 = %.2f\n", raiz1);
	printf("Raiz2 = %.2f\n", raiz2);

 	return 0;
}
