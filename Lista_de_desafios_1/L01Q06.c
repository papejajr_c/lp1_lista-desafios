//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>

int main(int argc, char const *argv[])
{
	int idade = 23;
	float altura = 1.57;
	char primeiraLetraNome = 'J';

 	printf("inteiro = %d\n", idade);
 	printf("real = %.2f\n", altura);
 	printf("caracter = %c\n", primeiraLetraNome);
	return 0;
}
