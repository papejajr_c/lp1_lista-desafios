//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	float nota1, nota2, nota3, media, mediaFinal, nota4;

 	printf("%s\n", "Digite a primeira nota");
	scanf("%f", &nota1);

	printf("%s\n", "Digite a segunda nota");
	scanf("%f", &nota2);

	printf("%s\n", "Digite a terceira nota");
	scanf("%f", &nota3);

	media = (nota1 * 2 + nota2 * 3 + nota3 * 4) / 9;

	if(media>=7) {
		printf("MP = %.2f\n", media);
		printf("Aprovado!\n");
	} else if(media>=3.5) {
		printf("Recuperação\n");
	
		nota4 = 70 - (media * 7);
		mediaFinal = (nota4 / 3) / 3;
		
		printf("%s = %.2f\n", "você precisa tirar", mediaFinal);
		
	} else {
		printf("%s\n", "Reprovado");
	}

 	return 0;
}
