//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	float nota1, nota2, nota3, media, mediaFinal, nota4;

 	printf("%s\n", "Digite a primeira nota");
	scanf("%f", &nota1);

	printf("%s\n", "Digite a segunda nota");
	scanf("%f", &nota2);

	printf("%s\n", "Digite a terceira nota");
	scanf("%f", &nota3);

	media = (nota1 * 2 + nota2 * 3 + nota3 * 4) / 9;

	if(media>=7) {
		printf("MP = %.2f\n", media);
		printf("Aprovado!\n");
	} else if(media>=3.5) {
		printf("Recuperação\n");
		printf("%s\n", "Digite a quarta nota");
		scanf("%f", &nota4);		
		mediaFinal = (media * 7 + nota4 * 3) / 10;
		printf("MF = %.2f\n", mediaFinal);
		if (mediaFinal >= 5)
			printf("Aprovado!\n");
		else
			printf("%s\n", "Reprovado");	
	} else {
		printf("%s\n", "Reprovado");
	}

 	return 0;
}
