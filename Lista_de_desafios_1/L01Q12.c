//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	const float precoRefrigerante = 1.5;
	const float precoPizza = 3.0;
	const float taxaGarcom = 0.10;

	int quantidadeRefrigerante, quantidadeFatiaPizza, quatidadePessoas;
	
	printf("%s\n", "Digite a quantidade de refrigerantes");
	scanf("%d", &quantidadeRefrigerante);

	printf("%s\n", "Digite a quantidade de fatias");
	scanf("%d", &quantidadeFatiaPizza);

	printf("%s\n", "Digite a quantidade de pessoas");
	scanf("%d", &quatidadePessoas);

	float totalRefrigerante = quantidadeRefrigerante * precoRefrigerante;
	float totalPizza = quantidadeFatiaPizza * precoPizza;
	float totalSemTaxa = totalPizza + totalRefrigerante;
	float totalComTaxa = totalSemTaxa + (totalSemTaxa * taxaGarcom);
	float rateioPorPessoa = totalComTaxa / quatidadePessoas;

 	printf("%s = %.2f\n", "total sem a taxa", totalSemTaxa);
 	printf("%s = %.2f\n", "total com a taxa", totalComTaxa);
	printf("%s = %.2f\n", "cada pessoa vai pagar", rateioPorPessoa);
 	
 	return 0;
}
