//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	int idade;
	float altura;
	char primeiraLetraNome;
	
	printf("%s\n", "Digite sua idade");
	scanf("%d", &idade);

	printf("%s\n", "Digite sua altura");
	scanf("%f", &altura);
 	
 	printf("%s\n", "Digite o primeiro caracter do seu nome");
	do
    	primeiraLetraNome = getchar();
	while (isspace(primeiraLetraNome));

 	printf("inteiro = %d\n", idade);
 	printf("real = %.2f\n", altura);
 	printf("caracter = %c\n", primeiraLetraNome);
	return 0;
}
