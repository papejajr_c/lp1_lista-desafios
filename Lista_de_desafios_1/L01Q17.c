//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	float numerador, denominador;

 	printf("%s\n", "Digite o numerador");
	scanf("%f", &numerador);

	printf("%s\n", "Digite o denominador");
	scanf("%f", &denominador);
	if(denominador == 0) {
		printf("denominador não pode ser zero\n");
		return 0;
	}
	printf("Resultado = %f\n", numerador/denominador);
		
 	return 0;
}
