//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char const *argv[])
{
	int numero;
	printf("%s\n", "Digite um numero");
	scanf("%d", &numero);
	printf("O raiz de %d : é %.0f\n", numero, sqrt(numero));
	return 0;
}
