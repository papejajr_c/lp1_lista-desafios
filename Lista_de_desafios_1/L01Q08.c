//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	int idade;
	float altura;
	char primeiraLetraNome;
	
	printf("%s\n", "Digite sua idade");
	scanf("%d", &idade);

	printf("%s\n", "Digite sua altura");
	scanf("%f", &altura);
 	
 	printf("%s\n", "Digite o primeiro caracter do seu nome");
	do
    	primeiraLetraNome = getchar();
	while (isspace(primeiraLetraNome));

 	printf("%d \t %.2f \t %c \n", idade, altura, primeiraLetraNome);
 	
	return 0;
}
