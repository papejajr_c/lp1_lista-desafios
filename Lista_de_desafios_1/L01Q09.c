//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	int num1, num2;
	
	printf("%s\n", "Digite o primeiro numero");
	scanf("%d", &num1);

	printf("%s\n", "Digite o segundo numero");
	scanf("%d", &num2);

 	printf("%s = %d\n", "adição", num1+num2);
 	printf("%s = %d\n", "subtração", num1-num2);
  	printf("%s = %d\n", "multiplicação", num1*num2);
  	printf("%s = %d\n", "quociente da divisão", num1/num2);
  	printf("%s = %d\n", "resto da divisão", num1%num2);
	return 0;
}
