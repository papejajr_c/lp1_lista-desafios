//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	printf("%s", (6 < 8) || (3 > 7) ? "verdadeiro": "falso");
	printf("\n");
	printf("%s", !(2 < 3) ? "verdadeiro": "falso");
	printf("\n");
	printf("%s", (5 >= 6 || 6 < 7) && !((3 + 5 * 6) == 48) ? "verdadeiro": "falso");
	printf("\n");
 	return 0;
}
