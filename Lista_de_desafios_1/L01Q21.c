//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	float altura, peso, resultado;

 	printf("%s\n", "Digite seu peso, em quilos");
	scanf("%f", &peso);

 	printf("%s\n", "Digite sua altura, em metros");
	scanf("%f", &altura);

	resultado = peso / (altura*altura);

	if(resultado < 18.5)
		printf("Seu peso é =  %f \tVocê está abaixo do peso ideal\n", resultado);
	else if(resultado < 24.9)
		printf("Seu peso é =  %f \tParabéns - Você está em seu peso normal\n", resultado);
	else if(resultado < 29.9)
		printf("Seu peso é =  %f \tVocê está acima de seu peso (sobrepeso)\n", resultado);
	else if(resultado < 34.9)
		printf("Seu peso é =  %f \tObesidade grau I\n", resultado);
	else if(resultado < 39.9)
		printf("Seu peso é =  %f \tObesidade grau II\n", resultado);
	else
		printf("Seu peso é =  %f \tObesidade grau III\n", resultado);

 	return 0;
}
