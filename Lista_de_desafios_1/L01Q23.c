//Agradeço a DEUS pelo dom do conhecimento
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	float conta1 = 100, conta2 = 100, valorTransferido;

 	printf("%s\n", "Digite o valor a ser transferido");
	scanf("%f", &valorTransferido);

	if (valorTransferido > conta1) {
		printf("Saldo insuficiente\n");
	}
	else {
		conta2+=valorTransferido;
		conta1-=valorTransferido;
	}

	printf("Saldo da conta1 = %.2f\n", conta1);
	printf("Saldo da conta2 = %.2f\n", conta2);
	
 	return 0;
}
